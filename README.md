# Paint Application - Rohit Programming Zone

Paint App realizada a partir del tutorial del canal de YouTube de **Rohit Programming Zone** ([link](https://www.youtube.com/watch?v=m7Ohm52TIhw&t=2s)).

## User Interface
<img src="./img/screenshot.png" alt="UI Screenshot" style="height: 500px"/>
